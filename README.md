An extremely lightweight Omeka plugin to display tags and basic tag filters in the admin/items/browse page, with the idea to use them to share and monitor indexation progress and indexations upcoming tasks.  

For a more comprehensive (and more demanding) interface dedicated to monitor items indexation, edition and publication, let me suggest [Curator Monitor](https://github.com/Daniel-KM/Omeka-plugin-CuratorMonitor) and [History Log](https://github.com/UCSCLibrary/HistoryLog) which the former requires.

Hyperlinks and CSS rules are very straightforwardly built in the `hookAdminItemsBrowse` function. They can very easily be modified to answer to any needs and tagging conventions.   

A bit of technical insight : this plugin relies on the `admin_items_browse` and `admin_items_browse_simple_each` hooks to inject additional html into admin items views. This is the recommanded approach to tweak Omeka admin interface. Although a cleaner approach than the one I had here would be to create partials templates and inject those instead of raw html (don't remember why I didn't proceed this way, guess there was a catch). 
