<?php


class TodoTagsPlugin extends Omeka_Plugin_AbstractPlugin
{

  /**
   * @var array Hooks for the plugin.
   */
    protected $_hooks = array(
      'admin_head',
      'admin_items_browse',
      'admin_items_browse_simple_each',
  );

    public function hookAdminItemsBrowseSimpleEach($args)
    {
        $item = $args['item'];
        $tags = tag_string('items');
        $html = '';
        if ($tags) {
            $splittedTags = explode(', ', $tags);
            foreach ($splittedTags as $tag) {
                $html .=" <span class='etiquette " . substr($tag, 34, 4). "'>" . $tag . "</span>";
            }
        }
        echo $html;
    }

    public function hookAdminHead()
    {
        queue_css_string(
            "
            .etiquette { font-weight: bold; margin-right:10px; }
            .DONE > a { color:green !important; }
            .TODO > a {  color:red !important; }
            .DISC > a {  color:orange !important; }
            .itemstodiscuss > a { color:orange; font-weight:bold; }
            .itemstoindex > a { color:red; font-weight:bold; }
            .itemsdone > a { color:green; font-weight:bold; }
            "
        );
    }


    public function hookAdminItemsBrowse($args)
    {
        $html = "";
        $tagList = [];
        $todoList = [];
        $discussList = [];
        $doneList = [];
        foreach (get_records('Tag', array(), 0) as $tag) {
            $tagList[$tag['name']] = "<a href='/admin/items/browse?tags=" . $tag['name'] . "'>" . $tag['name'] . " (" .  $tag['tagCount'] . ")" . "</a>";
        }
        arsort($tagList);

        foreach ($tagList as $tag => $val) {
            if (substr($tag, 0, 4) == "TODO") {
                $todoList[$tag] = $val;
            } elseif (substr($tag, 0, 4) == "DISC") {
                $discussList[$tag] = $val;
            } else {
                $doneList[$tag] = $val;
            }
        }

        $discuss = "<h2>A discuter</h2><ul>";
        foreach ($discussList as $k => $v) {
            $discuss .= "<li class='itemstodiscuss'>" . $v . "</li>";
        }
        $discuss .= "</ul>";

        $todo = "<h2>A compléter </h2><ul>";
        foreach ($todoList as $k => $v) {
            $todo .= "<li class='itemstoindex'>" . $v . "</li>";
        }
        $todo .= "</ul>";

        $done = "<h2>Terminés</h2><ul>";
        foreach ($doneList as $k => $v) {
            $done .= "<li class='itemsdone'>" . $v . "</li>";
        }
        $done .= "</ul>";

        $html .= $todo;
        $html .= $discuss;
        $html .= $done;

        echo $html ;
    }
}
